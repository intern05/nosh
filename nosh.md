# Nosh Accounts & Password Archive

## Nosh

### Internet

ISP: `TPG`

Username: `cre8fun2`

Password: `noshupabcd123`

### Netgear Router

IP: `110.174.14.216`

Remote Management: `https://110.174.14.216:8443`

Login: `192.168.1.1`

Username: `admin`

Password: `92124898`

WIFI Password: `13579com`

#### Port Forwarding

* POS `192.168.1.200:2134`
* NVR88 `192.168.1.99:88`
* NVR37777 `192.168.1.99:37777`
* NVR37778 `192.168.1.99:37778`
* NVR554 `192.168.1.99:554`

### D-Link AP

Login: http://dlinkap.local

Username: `admin`

Password: `www.acnw.com.au`

WIFI Password: `13579com`

### NVR

MAC Address: `90:02:A9:B8:97:03`

IP: `192.168.1.99`

Username: `admin`

Password: `enJoy8`

App: iDMSS Lite

### POS Remote Backend

Login: `110.174.14.216:2134`

----

## Shanghai Station Pty Ltd

### 银豹云后台

用户名: `David1234`

密码: `92124898`

### 银豹收银

用户名: `1001`

密码: `1001`

### iPad

iPad Unlock Pin: `1479`

iPad Restriction Pin: `4898`

### Apple ID

Apple ID (Australia): `it.acnw@gmail.com`

Apple ID (China): `op.itciti@gmail.com`

Password: `DavidApple123`

### NVR

UID: YVPD8EYRH4UUS8MC111A

Username: admin

Password: 123456

### Vend

Vend Domain: `noshpup.vendhq.com`

Vend Email: `it.acnw@gmail.com`

Vend Password: `acnwco92121479`

----

## China Square

### Internet

ISP: Telstra

Username: `chi27321980@direct.telstra.net`

Password: `34392`

Associated Email: `Chinasquared@hotmail.com`

### NVR

Username: `admin`

Password: `Ch1na2quar3D`

IP: 110.145.79.52

Port Forwarding: 8000

App: iVMS-4500

### POS Remote Backend

Login: `110.145.79.52:2134`

## ACNW Office

### Router

Login: admin

Password: www.acnw.com.au